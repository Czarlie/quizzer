/**
 * @template {keyof HTMLElementTagNameMap | typeof HTMLElement} TagName
 *     The tag name of the element
 */
export class ElementBuilder {
    /** @type {TagName} */
    #tagName;
    /** @type {string | null} */
    #id;
    /** @type {string[]} */
    #classes;
    /** @type {{[propertyName: string]: string}} */
    #style;
    /** @type {(
     *     string |
     *     ElementBuilder |
     *     HTMLElement |
     *     typeof HTMLElement |
     *     [typeof HTMLElement, *[]]
     * )[]} */
    #children;
    /** @type {{[attributeName: string]: string}} */
    #attributes;
    /** @type {{[attributeName: string]: *}} */
    #jsAttributes;

    /** @type {{[eventName: string]: (event) => *}} */
    #listeners;

    /**
     * Create a new ElementBuilder
     * @param {TagName} tagName The tag name or constructor of the element to build
     */
    constructor(tagName) {
        this.#tagName = tagName;
        this.#id = null;
        this.#classes = [];
        this.#style = {};
        this.#children = [];
        this.#attributes = {};
        this.#jsAttributes = {};
        this.#listeners = {};

        const setJSAttribute = (name, value) => {
            this.#jsAttributes[name] = value;
            return this;
        };

        const proxy = new Proxy(this, {
            get: (_target, name) => {
                if (name in this)
                    return {
                        [name]: (...args) => {
                            const ret = this[name](...args);
                            if (ret === this) return proxy;
                            return ret;
                        },
                    }[name];
                return {
                    [name]: (value) => {
                        setJSAttribute(name, value);
                        return proxy;
                    },
                }[name];
            },
        });

        return proxy;
    }

    /**
     * Set the ID of the element to build
     * @param  {string} id The id of the element
     * @return {this} The ElementBuilder itself
     */
    id(id) {
        this.#id = id;
        return this;
    }

    /**
     * Add a CSS class to the element to build
     * @param  {string} className The name of the class to add
     * @return {this} The ElementBuilder itself
     */
    class(className) {
        this.#classes.push(className);
        return this;
    }

    /**
     * Set a single style property on the element to build
     * @param  {string} name The name of the property to set
     * @param  {string} value The value to set the property to
     * @return {this} The ElementBuilder itself
     */
    styleProperty(name, value) {
        this.#style[name] = value;
        return this;
    }

    /**
     * Set style properties from an object.
     * @param  {{[name: string]: value}} properties The style properties to set
     * @return {this} The ElementBuilder itself
     */
    style(properties) {
        Object.entries(properties).forEach(([name, value]) =>
            this.styleProperty(name, value)
        );
        return this;
    }

    /**
     * Add a child to the element to build
     * @param {(
     *     string |
     *     ElementBuilder |
     *     HTMLElement |
     *     typeof HTMLElement |
     *     [typeof HTMLElement, *[]] |
     *     (
     *         string |
     *         ElementBuilder |
     *         HTMLElement |
     *         typeof HTMLElement |
     *         [typeof HTMLElement, *[]]
     *     )[]
     * )} child The child to add, either:
     *     <ul>
     *         <li> another ElementBuilder </li>
     *         <li> an element </li>
     *         <li> an element constructor </li>
     *         <li> an element constructor and arguments, in an array </li>
     *         <li> a string (to insert a text node) </li>
     *         <li> an array of any of the above, in any combination </li>
     *     </ul>
     * @return {this} The ElementBuilder itself
     */
    child(child) {
        if (
            Array.isArray(child) &&
            typeof child === "object" &&
            !(
                child.length === 2 &&
                child[0].prototype instanceof HTMLElement &&
                Array.isArray(child[1])
            )
        ) {
            this.#children.push(...child);
        } else {
            this.#children.push(child);
        }

        return this;
    }

    /**
     * Set a single attribute on the element to build
     * @param  {string} name The name of the attribute to set
     * @param  {string} value The value to set the attribute to
     * @return {this} The ElementBuilder itself
     */
    attribute(name, value) {
        this.#attributes[name] = value;
        return this;
    }

    /**
     * Add an event listener to the built element.
     * @template {keyof HTMLElementEventMap} EventType
     * @param  {EventType} event The event name to listen for
     * @param  {(event: HTMLElementEventMap[EventType]) => *} listener The listener to add
     * @return {this} The ElementBuilder itself
     */
    listen(event, listener) {
        this.#listeners[event] ??= [];
        this.#listeners[event].push(listener);
        return this;
    }

    /**
     * Build the specified element.
     * @return {(
     *     HTMLElement & (
     *         ElementTagNameMap[TagName & keyof ElementTagNameMap] |
     *         Exclude<TagName, keyof ElementTagNameMap>
     *     )
     * )} The built element
     */
    build() {
        /** @type {HTMLElement} */
        let element;
        if (typeof this.#tagName === "string")
            element = document.createElement(this.#tagName);
        else element = new this.#tagName();

        if (this.#id) element.id = this.#id;

        this.#classes.forEach((className) => element.classList.add(className));

        Object.entries(this.#attributes).forEach(([name, value]) =>
            element.setAttribute(name, value)
        );
        Object.entries(this.#jsAttributes).forEach(([name, value]) => {
            element[name] = value;
        });

        Object.entries(this.#listeners).forEach(([event, eventListeners]) =>
            eventListeners.forEach((listener) =>
                element.addEventListener(event, listener)
            )
        );

        Object.entries(this.#style).forEach(([name, value]) =>
            element.style.setProperty(name, value)
        );

        const appendFunc =
            element instanceof HTMLTemplateElement
                ? (child) => element.content.appendChild(child)
                : (child) => element.appendChild(child);

        this.#children.forEach((child) => {
            if (typeof child === "string")
                return appendFunc(document.createTextNode(child));

            if (Array.isArray(child))
                return appendFunc(new child[0](...child[1]));

            if (child instanceof this.constructor)
                return appendFunc(child.build());

            if (child instanceof HTMLElement) return appendFunc(child);

            if (child.prototype instanceof HTMLElement) appendFunc(new child());
        });

        return element;
    }
}
