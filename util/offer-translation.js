if (window.localStorage.getItem("translate")) {
    document.documentElement.classList.add(
        window.localStorage.getItem("translate")
    );
} else {
    window.localStorage.setItem("translate", "no-translate");
    document.documentElement.classList.add("no-translate");
}

document.querySelectorAll(".toggle-translate").forEach((el) =>
    el.addEventListener("click", () => {
        let offerTranslate = "no-translate";

        if (document.documentElement.classList.contains("translate"))
            offerTranslate = "translate";
        if (document.documentElement.classList.contains("no-translate"))
            offerTranslate = "no-translate";

        document.documentElement.classList.remove(offerTranslate);

        const setOfferTranslate = {
            translate: "no-translate",
            "no-translate": "translate",
        }[offerTranslate];
        document.documentElement.classList.add(setOfferTranslate);

        window.localStorage.setItem("translate", setOfferTranslate);
    })
);
