export const shorten = async (json) => {
    return await fetch("https://text-store.cza.li/store", {
        method: "POST",
        body: new URLSearchParams([["text", JSON.stringify(json)]]),
    }).then((response) => response.text());
};
export const lookup = async (key) => {
    return await fetch(`https://text-store.cza.li/${key}`)
        .then((response) => response.json())
        .catch(() => null);
};
