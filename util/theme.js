if (window.localStorage.getItem("theme"))
    document.documentElement.classList.add(
        window.localStorage.getItem("theme")
    );

document.querySelectorAll(".switch-theme").forEach((el) =>
    el.addEventListener("click", () => {
        let currentTheme = window.matchMedia("(prefers-color-scheme: light)")
            .matches
            ? "light"
            : "dark";

        if (document.documentElement.classList.contains("dark"))
            currentTheme = "dark";
        if (document.documentElement.classList.contains("light"))
            currentTheme = "light";

        document.documentElement.classList.remove(currentTheme);

        const setTheme = { light: "dark", dark: "light" }[currentTheme];
        document.documentElement.classList.add(setTheme);

        window.localStorage.setItem("theme", setTheme);
    })
);
