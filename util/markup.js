import { ElementBuilder } from "./element-builder.js";

/** @type {RegExp} */
const URL_REGEX =
    /((?:(?:[a-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[a-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[a-z0-9\.\-]+)(?:(?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_:]*)#?(?:[\.\!\/\\\w]*))?)/i;

const MARKUP_REGEX =
    /(?<before>(?:[^\\\*_`]|\\.)*)(?<delim>(?<!(?:\\{2})*\\)(?<delim_char>\*|_|`){1,2}(?!\k<delim_char>))(?<inside>(?:(?!\k<delim_char>)[^\\]|\\.|(?!\k<delim>(?!\k<delim_char>))\k<delim_char>+)*)(?<closing_delim>(?<!(?:\\{2})*\\)\k<delim>(?!\k<delim_char>))(?<after>.*)/;

/**
 * Parse markup and return marked up elements
 *
 * @param  {string} text The text to parse
 * @return {(string | HTMLAnchorElement)[]} An array of strings and elements: `<em>`, `<strong>` and `<a>`.
 */
export function parseMarkup(text) {
    const markupMatch = text.match(MARKUP_REGEX);

    if (markupMatch)
        return [
            ...parseMarkup(markupMatch.groups.before),
            new ElementBuilder(
                {
                    "*": "em",
                    "**": "strong",
                    _: "em",
                    __: "strong",
                    "`": "code",
                    "``": "code",
                }[markupMatch.groups.delim]
            )
                .child(
                    markupMatch.groups.delim !== "`"
                        ? parseMarkup(markupMatch.groups.inside)
                        : markupMatch.groups.inside
                )
                .build(),
            ...parseMarkup(markupMatch.groups.after),
        ];

    const segments = text.split(URL_REGEX);

    return segments.map((segment) => {
        let displayText = segment.replace(/^https?:\/\//, "");

        if (displayText.length > 20) {
            displayText = displayText.substring(0, 19) + "…";
        }

        return URL_REGEX.test(segment)
            ? new ElementBuilder("a")
                  .child(displayText)
                  .listen("click", (event) => {
                      event.stopPropagation();
                  })
                  .href(segment)
                  .build()
            : segment
                  .replace("\\*", "*")
                  .replace("\\_", "_")
                  .replace("\\`", "`");
    });
}
