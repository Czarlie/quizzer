import { lookup } from "./util/shortener.js";
import { Quiz } from "./model/quiz.js";
const playQuizElement = document.querySelector("qz-play-quiz");

const loadQuiz = () => {
    if (window.location.search.replace(/^\?/, "")) {
        lookup(window.location.search.replace(/^\?/, "")).then((json) => {
            if (!json) return;

            /** @type {Quiz} */
            const quiz = Quiz.deserialize(json);
            playQuizElement.quiz = quiz;

            const title = quiz.title.getLocalisation() || "<Untitled quiz>";

            document.title = `quizzer — ${title}`;
            document.querySelector("#quiz-title").innerText = title;
        });
    } else {
        window.location.replace("edit");
    }
};

window.addEventListener("hashchange", loadQuiz);
loadQuiz();
