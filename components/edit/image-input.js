import { Image } from "../../model/image.js";
import { ElementBuilder } from "../../util/element-builder.js";
import { LocalisedStringInput } from "./localised-string-input.js";
import { TabbedElement } from "./tabbed-element.js";

export class ImageInput extends TabbedElement {
    /** @type {Image} */
    #value;

    /** @type {{[name: string]: [attribute: string]}} */
    static #tabNameAttributes = {
        URL: "url",
        Caption: "caption",
        "Alt text": "alt",
        Attribution: "attribution",
    };

    /** @type {boolean} */
    #settingValue;

    contentFactory(tabName) {
        /** @type {ElementBuilder} */
        let attributeInputBuilder;
        /** @type {string} */
        const attrName = this.constructor.#tabNameAttributes[tabName];

        if (tabName !== "URL")
            attributeInputBuilder = new ElementBuilder(LocalisedStringInput);
        else
            attributeInputBuilder = new ElementBuilder("input")
                .type("url")
                .attribute("autocomplete", "photo")
                .value(this.#value[attrName]);

        let triggerUpdateTimeout = 0;

        /** @type {HTMLInputElement | LocalisedStringInput} */
        const attributeInput = attributeInputBuilder
            .id(`${attrName}-input`)
            .styleProperty("flex-grow", "1")
            .listen("input", () => {
                const value = attributeInput.value;
                const serializedValue =
                    typeof value === "string" ? value : value.serialize();

                this.#value = Image.deserialize({
                    ...this.#value.serialize(),
                    [attrName]: serializedValue,
                });

                if (!this.#settingValue) {
                    clearTimeout(triggerUpdateTimeout);
                    triggerUpdateTimeout = setTimeout(
                        () =>
                            attributeInput.dispatchEvent(
                                new CustomEvent("change")
                            ),
                        250
                    );
                }
            })
            .placeholder(tabName)
            .build();

        const containerBuilder = new ElementBuilder("div")
            .style({
                display: "flex",
                "flex-direction": "column",
                "align-items": "stretch",
                "justify-content": "stretch",
                // width: "50ch",
            })
            .child(attributeInput);

        if (tabName === "URL") {
            const imagePreview = new ElementBuilder("img")
                .id("preview")
                .alt("Image preview")
                .style({
                    border: "var(--btn-border)",
                    "border-radius": "var(--border-radius)",
                    "margin-top": "0.5rem",
                    "font-family": "sans-serif",
                    "max-width": "20rem",
                    "max-height": "30rem",
                    "object-fit": "contain",
                    background:
                        "repeating-conic-gradient(rgba(var(--fg-r), var(--fg-g), var(--fg-b), 0.125) 0% 25%, rgba(var(--fg-r), var(--fg-g), var(--fg-b), 0.25) 0% 50%) 50% / 20px 20px",
                })
                .build();

            attributeInput.addEventListener("change", () => {
                imagePreview.src = attributeInput.value;
            });

            containerBuilder.child(imagePreview);
        }

        return containerBuilder.build();
    }

    /** @param {Image} value */
    set value(value) {
        this.#settingValue = true;

        this.#value = Image.deserialize(value.serialize());

        Object.values(this.constructor.#tabNameAttributes).forEach(
            (imageAttribute) => {
                this.shadowRoot.querySelector(
                    `#${imageAttribute}-input`
                ).value = value[imageAttribute];
            }
        );

        this.shadowRoot.querySelector("#preview").src = this.#value.url;

        this.#settingValue = false;
        this.dispatchEvent(new CustomEvent("change"));
    }

    /** @return {Image} */
    get value() {
        return Image.deserialize(this.#value.serialize());
    }

    canRemove(name) {
        return false;
    }

    constructor() {
        super();

        this.showAddRegion = false;

        this.#value = new Image();
        this.#settingValue = false;

        Object.keys(this.constructor.#tabNameAttributes).forEach((tabName) =>
            this.addTab(tabName)
        );
        this.setActiveTab("URL", false);
    }
}

window.customElements.define("qz-image-input", ImageInput);
