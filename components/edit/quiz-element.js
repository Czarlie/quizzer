import { ElementBuilder } from "../../util/element-builder.js";
import { Quiz } from "../../model/quiz.js";
import { QuestionListElement } from "./question-list.js";
import { LocalisedStringInput } from "./localised-string-input.js";

export class QuizElement extends QuestionListElement {
    /** @type {LocalisedStringInput} */
    #titleInput;

    /** @type {HTMLInputElement} */
    #shuffleCheckbox;
    /** @type {HTMLInputElement} */
    #shuffleFromInput;

    /** @type {string} */
    #title;

    /** @type {boolean} */
    #settingValue;

    constructor() {
        super();

        this.#settingValue = false;

        this.shadowRoot
            .querySelector("#elements")
            .insertAdjacentElement(
                "beforebegin",
                new ElementBuilder("h1")
                    .child(
                        new ElementBuilder("span")
                            .id("quiz-title")
                            .child("<Untitled Quiz>")
                    )
                    .build()
            );

        this.shadowRoot
            .querySelector("#elements")
            .insertAdjacentElement(
                "beforebegin",
                new ElementBuilder("h2").child("Quiz title").build()
            );

        this.shadowRoot.querySelector("#elements").insertAdjacentElement(
            "beforebegin",
            new ElementBuilder("div")
                .id("title-section")
                .child(
                    (this.#titleInput = new ElementBuilder(LocalisedStringInput)
                        .listen("change", () => {
                            this.title =
                                this.#titleInput.value.getLocalisation("");

                            if (!this.#settingValue)
                                this.dispatchEvent(new CustomEvent("change"));
                        })
                        .placeholder("Quiz title")
                        .build())
                )
                .build()
        );

        this.shadowRoot
            .querySelector("#elements")
            .insertAdjacentElement(
                "beforebegin",
                new ElementBuilder("h2").child("Questions").build()
            );

        this.shadowRoot.querySelector("#elements").insertAdjacentElement(
            "beforebegin",
            new ElementBuilder("div")
                .id("shuffle-section")
                .child(
                    (this.#shuffleCheckbox = new ElementBuilder("input")
                        .id("shuffle-checkbox")
                        .type("checkbox")
                        .build())
                )
                .child(
                    new ElementBuilder("label")
                        .child("Shuffle questions ")
                        .child(
                            new ElementBuilder("span")
                                .class("from")
                                .child("starting at index")
                        )
                        .styleProperty("padding-left", "0.5rem")
                        .attribute("for", "shuffle-checkbox")
                        .styleProperty("margin-right", "0.5rem")
                )
                .child(
                    new ElementBuilder("section").child(
                        (this.#shuffleFromInput = new ElementBuilder("input")
                            .type("number")
                            .min(0)
                            .max(0)
                            .styleProperty("width", "6ch")
                            .disabled(true)
                            .build())
                    )
                )
                .build()
        );

        this.shadowRoot.prepend(
            new ElementBuilder("style")
                .child(
                    `
                    h1 {
                        max-width: 100%;
                        overflow: hidden;
                        text-overflow: ellipsis;
                    }

                    #title-section,
                    #title-section > * {
                        width: 100%;
                    }

                    #shuffle-section {
                        margin-bottom: 1rem;
                    }

                    #shuffle-section input[type="checkbox"] {
                        accent-color: rgba(
                            calc(var(--light) * var(--ac-r) + (1 - var(--light)) * (255 - var(--ac-r))),
                            calc(var(--light) * var(--ac-g) + (1 - var(--light)) * (255 - var(--ac-g))),
                            calc(var(--light) * var(--ac-b) + (1 - var(--light)) * (255 - var(--ac-b))),
                            var(--ac-a)
                        );

                        filter: invert(calc(1 - var(--light)));
                    }

                    #shuffle-checkbox:not(:checked) ~ section input,
                    #shuffle-checkbox:not(:checked) ~ label .from {
                        opacity: 0.66;
                        pointer-events: none;
                        --fg-a: 0.66;
                        color: var(--fg);
                    }

                    section {
                        display: inline-block;
                    }
                    `
                )
                .build()
        );

        [this.#shuffleCheckbox, this.#shuffleFromInput].forEach((input) =>
            input.addEventListener("change", () => {
                this.shuffleFrom = this.shuffleFrom;
            })
        );

        this.#shuffleFromInput.parentElement.addEventListener("click", () => {
            if (this.#shuffleCheckbox.checked) return;

            this.#shuffleCheckbox.checked = true;
            this.shuffleFrom = this.shuffleFrom;
            this.#shuffleFromInput.focus();
            this.#shuffleFromInput.select();
        });

        this.addEventListener("change", () => {
            this.#shuffleFromInput.max = Math.max(
                0,
                this.shadowRoot.querySelectorAll(
                    "#elements > .element-container"
                ).length - 1
            );
        });
    }

    /** @returns {number?} */
    get shuffleFrom() {
        if (!this.#shuffleCheckbox.checked) return null;

        // Prepending a "+" makes the empty string be parsed to NaN instead of 0
        // It also makes sure values are positive
        return Number.isInteger(+`+${this.#shuffleFromInput.value}`)
            ? Math.max(
                  Math.min(
                      +this.#shuffleFromInput.value,
                      super.value.length - 1
                  ),
                  0
              )
            : 0;
    }

    /** @param {number?} shuffleFrom */
    set shuffleFrom(shuffleFrom) {
        if (!Number.isInteger(shuffleFrom)) {
            this.#shuffleCheckbox.checked = false;
            this.#shuffleFromInput.disabled = true;
            this.#shuffleFromInput.value = "";
            return;
        }

        this.#shuffleCheckbox.checked = true;
        this.#shuffleFromInput.disabled = false;
        this.#shuffleFromInput.value = `${Math.max(
            Math.min(shuffleFrom, super.value.length - 1),
            0
        )}`;

        if (!this.#settingValue) this.dispatchEvent(new CustomEvent("change"));
    }

    get title() {
        return this.#title;
    }
    set title(title) {
        if (this.#title === title) return;

        const prevTitleElement = this.shadowRoot.querySelector("#quiz-title");
        prevTitleElement.insertAdjacentElement(
            "afterend",
            new ElementBuilder("span")
                .id("quiz-title")
                .child(`${title}` || "<Untitled Quiz>")
                .build()
        );
        prevTitleElement.remove();

        this.#title = title;
    }

    /** @returns {Quiz} */
    get value() {
        return new Quiz({
            title: this.#titleInput.value.serialize(),
            questions: super.value.map((question) => question.serialize()),
            shuffleFrom: this.shuffleFrom,
        });
    }

    /** @param {Quiz} value */
    set value(value) {
        this.shadowRoot.querySelector("#quiz-title").innerText =
            "Loading quiz...";
        this.shadowRoot.querySelector("#quiz-title").classList.add("loading");
        [...this.shadowRoot.children].forEach((child) =>
            child.style.setProperty("opacity", "0.66")
        );

        setTimeout(() => {
            super.value = value.questions;

            this.#settingValue = true;

            this.#titleInput.value = value.title;
            this.shuffleFrom = value.shuffleFrom;

            this.#settingValue = false;

            this.shadowRoot
                .querySelector("#quiz-title")
                .classList.remove("loading");
            [...this.shadowRoot.children].forEach((child) =>
                child.style.removeProperty("opacity")
            );
        }, 0);
    }
}

window.customElements.define("qz-quiz", QuizElement);
