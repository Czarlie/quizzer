import { Answer } from "../../model/answer.js";
import { ElementBuilder } from "../../util/element-builder.js";
import { BodySegmentInput } from "./body-segment-input.js";

export class AnswerInput extends BodySegmentInput {
    /** @type {HTMLInputElement} */
    #correctCheckbox;

    /** @type {boolean} */
    #settingValue;

    static placeholder = "Answer text";

    constructor() {
        super();

        this.#settingValue = false;

        this.shadowRoot.querySelector("#root-div").prepend(
            new ElementBuilder("div")
                .id("correct-section")
                .child(
                    (this.#correctCheckbox = new ElementBuilder("input")
                        .id("correct")
                        .type("checkbox")
                        .listen("change", () => {
                            if (!this.#settingValue)
                                this.dispatchEvent(new CustomEvent("change"));
                        })
                        .build())
                )
                .child(
                    new ElementBuilder("label")
                        .styleProperty("padding-left", "0.5rem")
                        .child("Correct Answer")
                        .attribute("for", "correct")
                )
                .build()
        );
        this.shadowRoot.prepend(
            new ElementBuilder("style")
                .child(
                    `
                    #correct-section {
                        padding-bottom: 0.5rem;
                    }

                    #correct-section input {
                        accent-color: rgba(
                            calc(var(--light) * var(--ac-r) + (1 - var(--light)) * (255 - var(--ac-r))),
                            calc(var(--light) * var(--ac-g) + (1 - var(--light)) * (255 - var(--ac-g))),
                            calc(var(--light) * var(--ac-b) + (1 - var(--light)) * (255 - var(--ac-b))),
                            var(--ac-a)
                        );

                        filter: invert(calc(1 - var(--light)));
                    }
                    `
                )
                .build()
        );
    }

    get correct() {
        return this.#correctCheckbox.checked;
    }

    set correct(correct) {
        this.#correctCheckbox.checked = correct;
    }

    /** @returns {Answer} */
    get value() {
        return new Answer({
            correct: this.#correctCheckbox.checked,
            body: super.value.serialize(),
        });
    }

    /** @param {Answer} value */
    set value(value) {
        this.#settingValue = true;
        this.correct = value.correct;
        this.#settingValue = false;

        super.value = value.body;
    }
}

window.customElements.define("qz-answer-input", AnswerInput);
