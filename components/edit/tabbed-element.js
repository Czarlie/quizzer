import { ElementBuilder } from "../../util/element-builder.js";

export class TabbedElement extends HTMLElement {
    /**
     * Create the content element for a new tab
     * @param  {string} name The tab name of the newly created tab
     * @param  {...*} args Arguments to the factory, default passes to ElementBuilder.child
     * @return {HTMLElement} An HTMLElement to add as the tab content
     */
    contentFactory(tabName, ...args) {
        const builder = new ElementBuilder("div");
        args.forEach((arg) => builder.child(arg));
        return builder.build();
    }

    /**
     * Return whether a tab name is valid
     * @param  {string} name The tab name to check
     * @return {boolean} Whether the tab name is valid, default: always true
     */
    validateTabName(name) {
        return true;
    }

    /**
     * Return whether a tab can be removed
     * @param  {string} name The tab name to check
     * @return {boolean} Whether the tab can be removed, default: always true
     */
    canRemove(name) {
        return true;
    }

    /** @type {{
     *     [name: string]: [
     *         HTMLButtonElement,
     *         HTMLDivElement
     *     ]
     * }} */
    #tabs;

    /** @type {HTMLDivElement} */
    #tabContainer;
    /** @type {HTMLDivElement} */
    #contentContainer;

    /** @type {string | null} */
    #activeTab;

    /** @type {boolean} */
    #showAddRegion;

    /** @type {boolean} */
    #showAsToggle;

    /** @type {boolean} */
    #showFrame;

    constructor() {
        super();

        this.#tabs = {};
        this.#activeTab = null;

        this.attachShadow({ mode: "open" });
        [...template.content.children].forEach((templateChild) =>
            this.shadowRoot.appendChild(templateChild.cloneNode(true))
        );

        this.#tabContainer = this.shadowRoot.querySelector("#tabs");
        this.#contentContainer = this.shadowRoot.querySelector("#contents");

        /** @type {HTMLFormElement} */
        this.addForm = this.shadowRoot.querySelector("#tab-add-form");
        this.addForm.addEventListener("submit", (e) => {
            e.preventDefault();

            if (!this.validateTabName(this.addInput.value)) {
                this.addInput.classList.add("error");
                this.addInput.focus();
                return;
            }

            if (this.addInput.value in this.#tabs) {
                this.activeTab = this.addInput.value;
                const [tabHandle, _] = this.#tabs[this.addInput.value];

                tabHandle.classList.add("force-focus");
                tabHandle.focus();

                this.addInput.value = "";

                return;
            }

            this.addTab(this.addInput.value);
            this.addInput.value = "";
        });

        /** @type {HTMLInputElement} */
        this.addInput = this.addForm.querySelector("#add-name");
        this.addInput.addEventListener("input", () => {
            if (
                this.addInput.value &&
                !this.validateTabName(this.addInput.value)
            )
                this.addInput.classList.add("error");
            else this.addInput.classList.remove("error");
        });
        this.addInput.addEventListener("focusin", () => {
            if (
                this.addInput.value &&
                !this.validateTabName(this.addInput.value)
            )
                this.addInput.classList.add("error");
            else this.addInput.classList.remove("error");
        });
        this.addInput.addEventListener("focusout", () => {
            this.addInput.classList.remove("error");
        });
        this.addInput.addEventListener("focusout", () => {
            this.addInput.classList.remove("error");
        });

        this.showAddRegion = true;
        this.showAsToggle = false;
        this.showFrame = false;
    }

    connectedCallback() {
        this.style.display = "inline-block";
    }

    get showAddRegion() {
        return this.#showAddRegion;
    }

    set showAddRegion(showAddRegion) {
        this.#showAddRegion = showAddRegion;

        if (showAddRegion) {
            delete this.addForm.style.display;
        } else {
            this.addForm.style.display = "none";
        }
    }

    get showAsToggle() {
        return this.#showAsToggle;
    }

    set showAsToggle(showAsToggle) {
        this.#showAsToggle = showAsToggle;

        if (showAsToggle) {
            this.#tabContainer.classList.remove("as-tabs");
            this.#tabContainer.classList.add("as-select");
            this.#contentContainer.classList.remove("as-tabs");
        } else {
            this.#tabContainer.classList.add("as-tabs");
            this.#tabContainer.classList.remove("as-select");
            this.#contentContainer.classList.add("as-tabs");
        }
    }

    get showFrame() {
        return this.#showFrame;
    }

    set showFrame(showFrame) {
        this.#showFrame = showFrame;

        if (showFrame) {
            this.shadowRoot.querySelector("#root-div").classList.add("frame");
        } else {
            this.shadowRoot
                .querySelector("#root-div")
                .classList.remove("frame");
        }
    }

    get activeTab() {
        return this.#activeTab;
    }

    set activeTab(tabName) {
        this.setActiveTab(tabName);
    }

    setActiveTab(tabName, scroll = true) {
        if (!(tabName in this.#tabs))
            throw new Error(
                `Cannot make a tab active that doesn't exist (tried "${tabName}")`
            );
        if (tabName === null)
            throw new Error(`Cannot make a tab \`${null}\` active`);

        if (tabName === this.#activeTab) return;

        if (this.#activeTab !== null) {
            const [oldTabHandle, oldContentContainer] =
                this.#tabs[this.#activeTab];

            oldTabHandle.classList.remove("active");
            oldTabHandle.disabled = false;
            oldContentContainer.classList.remove("active");
        }

        const [tabHandle, contentContainer] = this.#tabs[tabName];

        tabHandle.classList.add("active");
        tabHandle.disabled = true;
        contentContainer.classList.add("active");

        if (scroll)
            tabHandle.scrollIntoView({
                behavior: "auto",
                inline: "nearest",
                block: "nearest",
            });

        this.#activeTab = tabName;

        this.dispatchEvent(new CustomEvent("switch-tab"));
    }

    /**
     * Add a tab to the tabbed element, using `this.contentFactory` for the tab content
     * @param {string}    name The name of the tab. Must be unique.
     * @param {...*} args Arguments to pass to `this.contentFactory`
     */
    addTab(name, ...args) {
        if (name in this.#tabs)
            throw new Error(
                `Cannot add the same tab name twice (tried to add "${name}")`
            );

        /** @type {HTMLButtonElement} */
        const tabHandle = tabHandleTemplate.content.children[0].cloneNode(true);
        tabHandle
            .querySelector("slot.title")
            .replaceWith(document.createTextNode(name));
        tabHandle.addEventListener("click", () => {
            this.activeTab = name;
        });
        tabHandle.addEventListener("focusout", () =>
            tabHandle.classList.remove("force-focus")
        );

        /** @type {HTMLDivElement} */
        const contentWrapper =
            contentWrapperTemplate.content.children[0].cloneNode(true);
        contentWrapper.appendChild(this.contentFactory(name, ...args));

        this.#tabContainer.appendChild(tabHandle);
        this.#contentContainer.appendChild(contentWrapper);

        this.#tabs[name] = [tabHandle, contentWrapper];

        this.activeTab = name;

        this.dispatchEvent(new CustomEvent("update-tabs"));
    }

    removeTab(name, force = false) {
        if (!(name in this.#tabs))
            throw new Error(
                `Cannot remove a tab that doesn't exist (tried to remove "${name}")`
            );

        if (!this.canRemove(name) && !force) return;

        if (this.activeTab === name) {
            const newActiveTab = this.tabs[this.tabs.indexOf(name) - 1];
            if (newActiveTab !== undefined)
                this.setActiveTab(newActiveTab, false);
            else this.#activeTab = null;
        }

        const [tabHandle, contentContainer] = this.#tabs[name];
        tabHandle.remove();
        contentContainer.remove();
        delete this.#tabs[name];

        this.dispatchEvent(new CustomEvent("update-tabs"));
    }

    get tabs() {
        return Object.keys(this.#tabs);
    }
}

const template = new ElementBuilder("template")
    .child(
        new ElementBuilder("link")
            .rel("stylesheet")
            .type("text/css")
            .href(new URL("../../style/all.css", import.meta.url).toString())
    )
    .child(
        new ElementBuilder("style") //
            .child(`
                #contents .content:not(.active) {
                    display: none;
                }

                #contents.as-tabs {
                    padding: 0.5rem;
                    border: var(--btn-border);
                    border-radius: var(--border-radius);
                    border-top-left-radius: 0;
                }

                #root-div {
                    height: 100%;

                    display: flex;
                    flex-direction: column;
                    justify-content: stretch;
                }

                #root-div.frame {
                    padding: 0.5rem;
                    border: var(--btn-border);
                    border-radius: var(--border-radius);
                }

                #tab-row {
                    display: flex;
                    flex-direction: row;
                    align-content: stretch;
                    justify-content: space-between;
                    white-space: nowrap;
                }

                #tab-add-form * {
                    padding: 0.25rem 0.5rem;
                }

                #tabs {
                    overflow-x: auto;
                }
            `)
    )
    .child(
        new ElementBuilder("div")
            .id("root-div")
            .child(
                new ElementBuilder("div")
                    .id("tab-row")
                    .child(
                        new ElementBuilder("div")
                            .id("tabs")
                            .class("button-group")
                    )
                    .child(
                        new ElementBuilder("form")
                            .id("tab-add-form")
                            .class("button-group")
                            .child(
                                new ElementBuilder("input")
                                    .id("add-name")
                                    .class("tab")
                                    .styleProperty("max-width", "14ch")
                                    .attribute("autocomplete", "off")
                            )
                            .child(
                                new ElementBuilder("button")
                                    .id("add")
                                    .type("submit")
                                    .class("tab")
                                    .class("add")
                                    .child("Add")
                            )
                    )
            )
            .child(
                new ElementBuilder("div")
                    .id("contents")
                    .styleProperty("margin-top", "0.5rem")
            )
    )
    .build();

const tabHandleTemplate = new ElementBuilder("template")
    .child(
        new ElementBuilder("button")
            .class("tab")
            .child(new ElementBuilder("slot").class("title"))
    )
    .build();

const contentWrapperTemplate = new ElementBuilder("template")
    .child(new ElementBuilder("div").class("content"))
    .build();

window.customElements.define("qz-tabbed", TabbedElement);
