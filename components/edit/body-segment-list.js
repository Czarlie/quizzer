import { Image } from "../../model/image.js";
import { LocalisedString } from "../../model/localised-string.js";
import { ElementBuilder } from "../../util/element-builder.js";
import { BodySegmentInput } from "./body-segment-input.js";
import { EditableListElement } from "./editable-list.js";

export class BodySegmentListElement extends EditableListElement {
    itemFactory() {
        return new ElementBuilder(BodySegmentInput).build();
    }

    constructor() {
        super();

        this.orientation = "vertical";
        this.itemTypeText = "body segment";
    }

    /** @return {(Image | LocalisedString)[]} */
    get value() {
        return super.value;
    }

    /** @param {(Image | LocalisedString)[]} value */
    set value(value) {
        super.value = value;
    }
}

window.customElements.define("qz-body-segment-list", BodySegmentListElement);
