import { Question } from "../../model/question.js";
import { ElementBuilder } from "../../util/element-builder.js";
import { AnswerListElement } from "./answer-list.js";
import { BodySegmentListElement } from "./body-segment-list.js";
import { LocalisedStringInput } from "./localised-string-input.js";

/**
 * Check if an item is over 50% visible (meaning too far up or down to be seen)
 * @param  {HTMLElement} element
 * @return {boolean}
 */
const checkVisible = (element) => {
    const rect = element.getBoundingClientRect();
    const viewHeight = Math.max(
        document.documentElement.clientHeight,
        window.innerHeight
    );
    return !(
        rect.bottom - rect.height / 2 < 0 ||
        rect.top + rect.height / 2 - viewHeight >= 0
    );
};

export class QuestionElement extends HTMLElement {
    /** @type {boolean} */
    #collapsed;
    /** @type {number} */
    #index;
    /** @type {string} */
    #title;
    /** @type {boolean} */
    #showFrame;

    /** @type {HTMLDivElement} */
    #rootDiv;
    /** @type {HTMLHeadingElement} */
    #collapser;
    /** @type {HTMLDivElement} */
    #collapsible;
    /** @type {LocalisedStringInput} */
    #titleInput;
    /** @type {BodySegmentListElement} */
    #bodyList;
    /** @type {AnswerListElement} */
    #answersList;
    /** @type {HTMLButtonElement[]} */
    #deleteButtons;

    /** @type {boolean} */
    #settingValue;

    constructor() {
        super();

        this.#settingValue = false;

        this.attachShadow({ mode: "open" });
        [...template.content.children].forEach((templateChild) =>
            this.shadowRoot.appendChild(templateChild.cloneNode(true))
        );

        this.#rootDiv = this.shadowRoot.querySelector("#root-div");
        this.#collapser = this.shadowRoot.querySelector("#collapser");
        this.#collapsible = this.shadowRoot.querySelector("#collapsible");

        this.#titleInput = this.shadowRoot.querySelector(
            "#question-title-input"
        );
        this.#bodyList = this.shadowRoot.querySelector("#question-body");
        this.#answersList = this.shadowRoot.querySelector("#question-answers");
        this.#deleteButtons =
            this.shadowRoot.querySelectorAll(".delete-button");

        [this.#titleInput, this.#bodyList, this.#answersList].forEach((el) =>
            el.addEventListener("change", () => {
                if (!this.#settingValue)
                    this.dispatchEvent(new CustomEvent("change"));
            })
        );

        this.#rootDiv.addEventListener("transitionend", () => {
            if (this.#rootDiv.classList.contains("moving")) {
                this.#rootDiv.classList.remove("moving");

                if (!this.#rootDiv.classList.contains("collapsed"))
                    this.#rootDiv.scrollIntoView({
                        behavior: "smooth",
                        block: "start",
                    });
            }
        });
        this.#collapser.addEventListener("click", () => {
            this.collapsed = !this.collapsed;
        });
        this.#titleInput.addEventListener("change", () => {
            this.title = this.#titleInput.value.getLocalisation("");
        });

        this.#deleteButtons.forEach((button) =>
            button.addEventListener("click", () => {
                this.dispatchEvent(new CustomEvent("delete"));
            })
        );

        const resizeObserver = new ResizeObserver(() => {
            this.#rootDiv.style.setProperty(
                "--collapser-height",
                `calc(${this.#collapser.getBoundingClientRect().height}px + ${
                    window.getComputedStyle(this.#collapser).marginTop
                }`
            );
            this.#rootDiv.style.setProperty(
                "--collapsible-height",
                `${this.#collapsible.offsetHeight}px + ${
                    window.getComputedStyle(this.#collapsible).marginBottom
                } + ${window.getComputedStyle(this.#collapsible).marginTop} + ${
                    window.getComputedStyle(this.#collapser).marginBottom
                } `
            );
        });
        resizeObserver.observe(this.#collapser);
        resizeObserver.observe(this.#collapsible);

        this.index = -1;
        this.title = "";
        this.collapsed = false;
        this.#rootDiv.classList.remove("moving");
        this.showFrame = true;
    }

    get collapsed() {
        return this.#collapsed;
    }
    set collapsed(collapsed) {
        if (this.#collapsed === collapsed) return;

        this.#rootDiv.classList.add("moving");

        if (collapsed) {
            this.#rootDiv.classList.add("collapsed");
        } else {
            this.#rootDiv.classList.remove("collapsed");
        }

        this.#collapsed = collapsed;
    }

    get index() {
        return this.#index;
    }
    set index(index) {
        if (this.#index === index) return;

        const prevIndexElement =
            this.shadowRoot.querySelector("#question-index");
        prevIndexElement.insertAdjacentElement(
            "afterend",
            new ElementBuilder("span")
                .id("question-index")
                .child(`${index}`)
                .build()
        );
        prevIndexElement.remove();

        this.#index = index;
    }

    get title() {
        return this.#title;
    }
    set title(title) {
        if (this.#title === title) return;

        const prevTitleElement =
            this.shadowRoot.querySelector("#question-title");
        prevTitleElement.insertAdjacentElement(
            "afterend",
            new ElementBuilder("span")
                .id("question-title")
                .child(`${title}` || "<Untitled>")
                .build()
        );
        prevTitleElement.remove();

        this.#title = title;
    }

    get showFrame() {
        return this.#showFrame;
    }

    set showFrame(showFrame) {
        this.#showFrame = showFrame;

        if (showFrame) {
            this.shadowRoot.querySelector("#root-div").classList.add("frame");
        } else {
            this.shadowRoot
                .querySelector("#root-div")
                .classList.remove("frame");
        }
    }

    /** @return {Question} */
    get value() {
        return new Question({
            title: this.#titleInput.value.serialize(),
            body: this.#bodyList.value.map((bodySegment) =>
                bodySegment.serialize()
            ),
            answers: this.#answersList.value.map((answer) =>
                answer.serialize()
            ),
        });
    }

    /** @param {Question} value */
    set value(value) {
        this.#settingValue = true;

        this.#titleInput.value = value.title;
        this.#bodyList.value = value.body;
        this.#answersList.value = value.answers;

        this.#settingValue = false;
        this.dispatchEvent(new CustomEvent("change"));
    }
}

const template = new ElementBuilder("template")
    .child(
        new ElementBuilder("link")
            .rel("stylesheet")
            .type("text/css")
            .href(new URL("../../style/all.css", import.meta.url).toString())
    )
    .child(
        new ElementBuilder("style") //
            .child(`
                #root-div {
                    overflow: hidden;
                    box-sizing: content-box;
                    max-width: 100%;
                }
                #root-div.moving {
                    transition: max-height 0.2s ease;
                }

                #root-div.frame {
                    padding: 0.5rem;
                    padding-left: 2.5rem;
                    border: var(--btn-border);
                    border-radius: var(--border-radius);
                }

                #root-div:not(.collapsed) {
                    max-height: calc(var(--collapser-height) + var(--collapsible-height));
                }
                #root-div.collapsed {
                    max-height: var(--collapser-height);
                }

                #root-div:not(.collapsed) #collapser {
                    cursor: n-resize;
                }

                #root-div.collapsed #collapser {
                    cursor: s-resize;
                }

                #collapsible {
                    padding: 0.5rem 0 0.5rem 0.6rem;
                    border-left: var(--btn-border);
                    border-radius: var(--border-width);
                }

                #collapser-section {
                    display: flex;
                    flex-direction: row;
                    justify-content: stretch;
                    gap: 0.5rem;
                }
                #collapser-section > * {
                    margin-bottom: 0.5rem;
                }

                #collapser {
                    flex-grow: 1;
                    text-align: left;
                    font-size: 125%;
                    font-weight: bold;
                }

                #full-delete-button {
                    margin-top: 2rem;
                }

                #small-delete-button {
                    max-width: 100px;
                }

                #root-div:not(.collapsed) #small-delete-button {
                    content: none;
                    max-width: 0;
                    padding: 0;
                    border-left-width: 0;
                    border-right-width: 0;
                    margin-left: -0.5rem;
                    overflow: hidden;
                }
            `)
    )
    .child(
        new ElementBuilder("div")
            .id("root-div")
            .child(
                new ElementBuilder("div")
                    .id("collapser-section")
                    .child(
                        new ElementBuilder("button")
                            .id("collapser")
                            .attribute(
                                "aria-label",
                                "Collapse/expand this question"
                            )
                            .child("Question ")
                            .child(
                                new ElementBuilder("slot").id("question-index")
                            )
                            .child(": ")
                            .child(
                                new ElementBuilder("slot").id("question-title")
                            )
                    )
                    .child(
                        new ElementBuilder("button")
                            .id("small-delete-button")
                            .attribute("aria-label", "Delete this question")
                            .class("error")
                            .class("delete")
                            .class("delete-button")
                    )
            )
            .child(
                new ElementBuilder("div")
                    .id("collapsible")
                    .child(
                        new ElementBuilder("h3")
                            .styleProperty("margin-top", "0")
                            .styleProperty("margin-bottom", "0.5rem")
                            .child("Question Title:")
                    )
                    .child(
                        new ElementBuilder(LocalisedStringInput)
                            .styleProperty("box-sizing", "border-box")
                            .styleProperty("width", "100%")
                            .placeholder("Question title")
                            .id("question-title-input")
                    )
                    .child(
                        new ElementBuilder("h3")
                            .styleProperty("margin-top", "1rem")
                            .styleProperty("margin-bottom", "0.5rem")
                            .child("Body:")
                    )
                    .child(
                        new ElementBuilder(BodySegmentListElement)
                            .styleProperty("box-sizing", "border-box")
                            .styleProperty("width", "100%")
                            .id("question-body")
                    )
                    .child(
                        new ElementBuilder("h3")
                            .styleProperty("margin-top", "1rem")
                            .styleProperty("margin-bottom", "0.5rem")
                            .child("Answers:")
                    )
                    .child(
                        new ElementBuilder(AnswerListElement)
                            .styleProperty("box-sizing", "border-box")
                            .styleProperty("width", "100%")
                            .id("question-answers")
                    )
                    .child(
                        new ElementBuilder("button")
                            .child("Delete question")
                            .class("error")
                            .class("delete")
                            .id("full-delete-button")
                            .class("delete-button")
                    )
            )
    )
    .build();

window.customElements.define("qz-question", QuestionElement);
