import { Answer } from "../../model/answer.js";
import { ElementBuilder } from "../../util/element-builder.js";
import { AnswerInput } from "./answer-input.js";
import { EditableListElement } from "./editable-list.js";

export class AnswerListElement extends EditableListElement {
    itemFactory() {
        return new ElementBuilder(AnswerInput).build();
    }

    constructor() {
        super();

        this.orientation = "horizontal";
        this.itemTypeText = "answer";
    }

    /** @return {Answer[]} */
    get value() {
        return super.value;
    }

    /** @param {Answer[]} value */
    set value(value) {
        super.value = value;
    }
}

window.customElements.define("qz-answer-list", AnswerListElement);
