import { ElementBuilder } from "../../util/element-builder.js";

import { Sortable } from "../../node_modules/sortablejs/modular/sortable.esm.js";

/**
 * Check if an item is over 50% visible (meaning too far up or down to be seen)
 * @param  {HTMLElement} element
 * @return {boolean}
 */
const checkVisible = (element) => {
    const rect = element.getBoundingClientRect();
    const viewHeight = Math.max(
        document.documentElement.clientHeight,
        window.innerHeight
    );
    return !(
        rect.bottom - rect.height / 2 < 0 ||
        rect.top + rect.height / 2 - viewHeight >= 0
    );
};

export class EditableListElement extends HTMLElement {
    /** @type {HTMLButtonElement} */
    #addButton;
    /** @type {HTMLDivElement} */
    #elementsContainer;

    /** @type {Sortable} */
    #sortable;

    /** @type {string} */
    #itemTypeText;

    /** @type {Map<HTMLElement, number>} */
    #itemIndices;

    /** @type {"horizontal" | "vertical"} */
    #orientation;

    /** @type {boolean} */
    #settingValue;

    /**
     * Create a new element that can be used as an item for this EditableListElement.
     * @return {HTMLElement}
     */
    itemFactory() {
        return new ElementBuilder("h1").child("item").build();
    }

    /**
     * Get the value for an item for returning from `get value`.
     * @param  {HTMLElement} item The element to get a value for
     * @return {*} Any value that the item may represent
     */
    getItemValue(item) {
        return item?.value;
    }

    /** @return {*[]} */
    get value() {
        return [...this.#elementsContainer.children].map((item) => {
            if (Number.isInteger(this.#itemIndices.get(item)))
                return this.getItemValue(item.children[1]);

            return this.getItemValue(item);
        });
    }

    /**
     * Warning: assumes that each item produced the item factory has a settable value attribute.
     * @param {*[]} value
     */
    set value(value) {
        this.#settingValue = true;

        [...this.#elementsContainer.children].forEach((child) =>
            child.remove()
        );
        this.#itemIndices = new Map();

        value.forEach((item) => {
            this.addItem().value = item;
        });

        this.#settingValue = false;
        this.dispatchEvent(new CustomEvent("change"));
    }

    constructor() {
        super();

        this.#settingValue = false;

        this.attachShadow({ mode: "open" });
        [...template.content.children].forEach((templateChild) =>
            this.shadowRoot.appendChild(templateChild.cloneNode(true))
        );

        this.#itemIndices = new Map();

        this.#addButton = this.shadowRoot.querySelector("#add-button");
        this.#elementsContainer = this.shadowRoot.querySelector("#elements");

        this.#sortable = new Sortable(this.#elementsContainer, {
            handle: ".handle",
            animation: 200,
            onUpdate: ({ oldIndex, newIndex }) => {
                [...this.#elementsContainer.children].forEach((item, index) => {
                    this.#itemIndices.set(item, index);
                    item.children[1].index = index;
                });

                if (!this.#settingValue)
                    this.dispatchEvent(new CustomEvent("change"));
            },
        });

        this.#addButton.addEventListener("click", () => {
            const addElementObserver = new MutationObserver((records) => {
                records.forEach((record) => {
                    if (!record.addedNodes?.length) return;

                    addElementObserver.disconnect();

                    // Scroll into view after add
                    if (
                        !checkVisible(this.#addButton) ||
                        !checkVisible(addedItem)
                    )
                        this.scrollIntoView({
                            behavior: "smooth",
                            block: "nearest",
                        });
                });
            });
            addElementObserver.observe(this.#elementsContainer, {
                childList: true,
            });
            const addedItem = this.addItem();
        });

        this.itemTypeText = "item";
        this.orientation = "horizontal";
    }

    /**
     * Produce a new item with the item factory, add it and then return it.
     * @returns {HTMLElement} the added item
     */
    addItem() {
        const itemElement = this.itemFactory();

        itemElement.addEventListener("delete", () =>
            this.removeItem(itemElement)
        );
        itemElement.addEventListener("change", () => {
            if (!this.#settingValue)
                this.dispatchEvent(new CustomEvent("change"));
        });
        new ResizeObserver(() => {
            if (
                new Set(
                    [...this.#elementsContainer.children].map(
                        (element) =>
                            element.children[1].getBoundingClientRect().width
                    )
                ).size <= 1
            ) {
                this.#elementsContainer.style.setProperty(
                    "--item-width",
                    "unset"
                );
                return;
            }

            // Introduce a slight difference (-0.01) between the widest element
            // and the min-width so that elements aren't exactly equal unless:
            // a) they would be equal naturally, meaning a homogenous min-width
            //    does not have any effects anyways
            // b) there is no longer an element that would be as large as the
            //    set min-width and the only thing keeping the elements from
            //    being smaller is the set min-width, in which case we want to
            //    unset the min-width to be able to recalculate it with the
            //    elements' natural widths.
            this.#elementsContainer.style.setProperty(
                "--item-width",
                `${
                    Math.max(
                        ...[...this.#elementsContainer.children].map(
                            (element) =>
                                element.children[1].getBoundingClientRect()
                                    .width
                        )
                    ) - 0.01
                }px`
            );
        }).observe(itemElement, {
            box: "border-box",
        });

        const container = new ElementBuilder("div")
            .class("element-container")
            .child(
                new ElementBuilder("div")
                    .class("handle-container")
                    .child(new ElementBuilder("div").class("handle"))
            )
            .child(itemElement)
            .styleProperty("max-width", "100%")
            .build();

        this.#itemIndices.set(
            container,
            this.#elementsContainer.children.length
        );
        itemElement.index = this.#itemIndices.get(container);

        this.#elementsContainer.append(container);

        if (!this.#settingValue) this.dispatchEvent(new CustomEvent("change"));

        return itemElement;
    }

    /**
     * Remove an item by reference or by index
     * @param  {number | HTMLElement} item The item to remove
     */
    removeItem(item) {
        let index;

        if (typeof item === "number") {
            index = item;
            item = this.#elementsContainer.children[index];
        } else {
            item = item.closest("#elements > .element-container");
            index = this.#itemIndices.get(item);
            if (!Number.isInteger(index))
                throw new Error(
                    `Tried to remove nonexistant item ${item} from EditableListElement`
                );
        }

        item.remove();

        [...this.#elementsContainer.children].forEach((item, index) => {
            this.#itemIndices.set(item, index);
            item.children[1].index = index;
        });

        delete this.#itemIndices.get(item);

        if (!this.#settingValue) this.dispatchEvent(new CustomEvent("change"));
    }

    get itemTypeText() {
        return this.#itemTypeText;
    }

    set itemTypeText(itemTypeText) {
        const previousText = this.shadowRoot.querySelector("#item-type-text");
        previousText.insertAdjacentElement(
            "afterend",
            new ElementBuilder("span")
                .id("item-type-text")
                .child(`${itemTypeText}`)
                .build()
        );
        previousText.remove();
        this.#itemTypeText = itemTypeText;
    }

    get orientation() {
        return this.#orientation;
    }

    /**
     * Set the orientation of this list.
     * @param  {"horizontal" | "vertical"} orientation
     */
    set orientation(orientation) {
        this.#elementsContainer.style.flexDirection =
            orientation === "horizontal" ? "row" : "column";
        this.#elementsContainer.style.float =
            orientation === "horizontal" ? "left" : "none";

        this.style.flexDirection =
            orientation === "horizontal" ? "row" : "column";
        this.style.alignItems =
            orientation === "horizontal" ? "center" : "start";

        this.#elementsContainer.classList.remove(
            orientation === "horizontal" ? "vertical" : "horizontal"
        );
        this.#elementsContainer.classList.add(orientation);

        this.#orientation = orientation;
    }
}

const template = new ElementBuilder("template")
    .child(
        new ElementBuilder("link")
            .rel("stylesheet")
            .type("text/css")
            .href(new URL("../../style/all.css", import.meta.url).toString())
    )
    .child(
        new ElementBuilder("style") //
            .child(`
                :host {
                    display: flex;
                    flex-wrap: wrap;
                }
                #elements {
                    display: flex;
                    justify-content: start;
                    align-items: stretch;
                    gap: 0.5rem;
                    max-width: 100%;
                    flex-wrap: wrap;
                    --item-width: 0px;
                }
                #elements.horizontal > * > :last-child {
                    min-width: var(--item-width);
                }
                #elements.vertical,
                #elements.vertical > * > :last-child {
                    width: 100%;
                }

                #elements > * {
                    display: flex;
                    flex-direction: row;
                    align-items: stretch;
                }

                #elements > * > :last-child {
                    height: 100%;
                }

                #elements > * > .handle-container:first-child {
                    width: 0;
                    overflow: visible;
                    float: left;
                }

                #elements > * > .handle-container:first-child > .handle {
                    position: relative;
                    margin: var(--border-width);

                    box-sizing: border-box;

                    width: 2rem;
                    height: calc(100% - 2 * var(--border-width));
                    padding: 0.5rem;

                    cursor: grab;

                    background: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='100' height='100'%3E%3Ccircle cx='50' cy='50' r='25' fill='%23888'/%3E%3C/svg%3E");
                    background-size: 0.5rem 0.5rem;
                    background-clip: content-box;
                    background-repeat: space;
                }

                #elements:not(:empty)::after {
                    content: " ";
                    width: 0;
                }
                #elements.horizontal:not(:empty) {
                    margin-bottom: 0.5rem;
                }
            `)
    )
    .child(new ElementBuilder("div").id("elements"))
    .child(
        new ElementBuilder("button")
            .id("add-button")
            .class("add")
            .child("Add ")
            .child(new ElementBuilder("slot").id("item-type-text"))
    )
    .build();

window.customElements.define("qz-editable-list", EditableListElement);
