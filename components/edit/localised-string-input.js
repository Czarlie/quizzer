import { LocalisedString } from "../../model/localised-string.js";
import { ElementBuilder } from "../../util/element-builder.js";
import { TabbedElement } from "./tabbed-element.js";

export class LocalisedStringInput extends TabbedElement {
    /** @type {LocalisedString} */
    #value;

    /** @type {string} */
    #placeholder;

    /** @type {{[lang: string]: HTMLInputElement}} */
    #langInputs;

    /** @type {boolean} */
    #settingValue;

    contentFactory(tabName) {
        /** @type {HTMLInputElement} */
        const langInput = new ElementBuilder("input")
            .type("text")
            .lang(tabName)
            .value(
                this.#value.getLocalisation(
                    tabName === "default" ? "" : tabName
                )
            )
            .placeholder(`${this.#placeholder} for locale "${tabName}"`)
            .styleProperty("flex-grow", "1")
            .listen("input", () => {
                this.#value.setLocalisation(
                    tabName === "default" ? "" : tabName,
                    langInput.value
                );

                this.dispatchEvent(new CustomEvent("input"));
                this.dispatchEvent(new CustomEvent("change"));
            })
            .build();

        this.#langInputs[tabName] = langInput;

        /** @type {HTMLInputElement} */
        const deleteButton = new ElementBuilder("button")
            .child("Delete")
            .class("error")
            .class("delete")
            .styleProperty("flex-grow", "0")
            .listen("click", () => this.removeTab(tabName))
            .build();

        const containerBuilder = new ElementBuilder("div")
            .style({
                display: "flex",
                "flex-direction": "row",
                "align-items": "stretch",
                "justify-content": "stretch",
                gap: "0.5rem",
                width: "100%",
            })
            .child(langInput);

        if (this.canRemove(tabName)) containerBuilder.child(deleteButton);

        return containerBuilder.build();
    }

    validateTabName(name) {
        return name !== "" && LocalisedString.checkLangTag(name);
    }

    canRemove(name) {
        return name !== "default";
    }

    constructor() {
        super();

        this.#langInputs = {};

        this.placeholder = "String";

        this.addInput.placeholder = "Add translation";
        this.addInput.pattern = "[a-z]{2,3}(-([A-Z]{2}|\\d{3}))?";
        this.addInput.title = 'A language tag, like "en", "en-GB"';

        this.value = new LocalisedString("");

        this.shadowRoot.querySelector("#root-div").style.maxWidth =
            "var(--translation-tab-display, 100%)";

        this.shadowRoot.querySelector("#tab-row").style.display =
            "var(--translation-tab-display, flex)";
        this.shadowRoot.querySelector("#contents").style.padding =
            "calc(0.5rem * var(--translation-width-scale, 1))";
        this.shadowRoot.querySelector("#contents").style.marginTop =
            "calc(0.5rem * var(--translation-width-scale, 1))";
        this.shadowRoot.querySelector("#contents").style.borderWidth =
            "calc(var(--border-width) * var(--translation-width-scale, 1))";

        this.#settingValue = false;
        this.addEventListener("update-tabs", () => {
            if (!this.#settingValue) {
                this.dispatchEvent(new CustomEvent("input"));
                this.dispatchEvent(new CustomEvent("change"));
            }
        });
    }

    /** @param {LocalisedString} value */
    set value(value) {
        this.#settingValue = true;

        this.#value = LocalisedString.deserialize(value.serialize());

        this.tabs.forEach((tabName) => {
            this.removeTab(tabName, true);
        });

        this.#value.getLanguages().forEach((lang) => {
            this.addTab(lang === "" ? "default" : lang);
        });

        this.setActiveTab("default", false);

        this.#settingValue = false;
        this.dispatchEvent(new CustomEvent("input"));
        this.dispatchEvent(new CustomEvent("change"));
    }
    /** @return {LocalisedString} */
    get value() {
        return LocalisedString.deserialize(this.#value.serialize());
    }

    /** @param {string} placeholder */
    set placeholder(placeholder) {
        this.#placeholder = placeholder;

        Object.entries(this.#langInputs).forEach(([lang, langInput]) => {
            langInput.placeholder = `${this.#placeholder} for locale "${lang}"`;
        });
    }
    /** @return {string} */
    get placeholder() {
        return this.#placeholder;
    }
}

window.customElements.define("qz-localised-string-input", LocalisedStringInput);
