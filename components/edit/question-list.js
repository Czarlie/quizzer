import { ElementBuilder } from "../../util/element-builder.js";
import { QuestionElement } from "./question-element.js";
import { EditableListElement } from "./editable-list.js";

export class QuestionListElement extends EditableListElement {
    /** @type {boolean} */
    #valueSet;

    itemFactory() {
        return new ElementBuilder(QuestionElement)
            .collapsed(!this.#valueSet)
            .build();
    }

    constructor() {
        super();

        this.#valueSet = false;

        this.orientation = "vertical";
        this.itemTypeText = "Question";
    }

    /** @return {Question[]} */
    get value() {
        return super.value;
    }

    /** @param {Question[]} value */
    set value(value) {
        super.value = value;

        this.#valueSet = true;
    }
}

window.customElements.define("qz-question-list", QuestionListElement);
