import { Image } from "../../model/image.js";
import { LocalisedString } from "../../model/localised-string.js";
import { ElementBuilder } from "../../util/element-builder.js";
import { ImageInput } from "./image-input.js";
import { LocalisedStringInput } from "./localised-string-input.js";
import { TabbedElement } from "./tabbed-element.js";

export class BodySegmentInput extends TabbedElement {
    /** @type {LocalisedStringInput} */
    #textInput;
    /** @type {ImageInput} */
    #imageInput;

    /** @type {boolean} */
    #settingValue;

    static placeholder = "Text";

    contentFactory(tabName) {
        /** @type {LocalisedStringInput | ImageInput} */
        let contentInput;

        switch (tabName) {
            case "Text":
                contentInput = new ElementBuilder(LocalisedStringInput)
                    .id("text-input")
                    .placeholder(this.constructor.placeholder)
                    .styleProperty("width", "100%")
                    .listen("change", () => {
                        if (!this.#settingValue)
                            this.dispatchEvent(new CustomEvent("change"));
                    })
                    .build();
                break;
            case "Image":
                contentInput = new ElementBuilder(ImageInput)
                    .id("image-input")
                    .styleProperty("width", "100%")
                    .listen("change", () => {
                        if (!this.#settingValue)
                            this.dispatchEvent(new CustomEvent("change"));
                    })
                    .build();
                break;
        }

        return contentInput;
    }

    constructor() {
        super();

        this.showAddRegion = false;
        this.showAsToggle = true;
        this.showFrame = true;

        this.#settingValue = false;

        this.addTab("Text");
        this.addTab("Image");
        this.setActiveTab("Text", false);

        this.#textInput = this.shadowRoot.querySelector("#text-input");
        this.#imageInput = this.shadowRoot.querySelector("#image-input");

        this.addEventListener("switch-tab", () => {
            if (!this.#settingValue)
                this.dispatchEvent(new CustomEvent("change"));
        });

        const root = this.shadowRoot.querySelector("#root-div");
        root.append(
            new ElementBuilder("div")
                .id("delete-section")
                .styleProperty("padding-top", "0.5rem")
                .styleProperty("height", "100%")
                .styleProperty("display", "flex")
                .styleProperty("flex-direction", "column")
                .styleProperty("align-items", "start")
                .styleProperty("justify-content", "end")
                .child(
                    new ElementBuilder("button")
                        .child("Delete")
                        .class("error")
                        .class("delete")
                        .listen("click", () =>
                            this.dispatchEvent(new CustomEvent("delete"))
                        )
                        .build()
                )
                .build()
        );

        root.style.paddingLeft = "2.5rem";
    }

    /** @return {LocalisedString | Image} */
    get value() {
        if (this.activeTab === "Text") {
            return LocalisedString.deserialize(
                this.#textInput.value.serialize()
            );
        }
        return Image.deserialize(this.#imageInput.value.serialize());
    }

    /** @param {LocalisedString | Image} */
    set value(value) {
        this.#settingValue = true;

        this.setActiveTab(
            value instanceof LocalisedString ? "Text" : "Image",
            false
        );

        (value instanceof LocalisedString
            ? this.#textInput
            : this.#imageInput
        ).value = value;

        this.#settingValue = false;
        this.dispatchEvent(new CustomEvent("change"));
    }
}

window.customElements.define("qz-body-segment", BodySegmentInput);
