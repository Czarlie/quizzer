import { ElementBuilder } from "../../util/element-builder.js";
import { parseMarkup } from "../../util/markup.js";

import { Question } from "../../model/question.js";
import { LocalisedString } from "../../model/localised-string.js";

export class PlayQuestionElement extends HTMLElement {
    /** @type {Question} */
    #question = null;

    constructor() {
        super();

        this.attachShadow({ mode: "open" });
        [...template.content.children].forEach((templateChild) =>
            this.shadowRoot.appendChild(templateChild.cloneNode(true))
        );
    }

    /** @returns {Question} */
    get question() {
        // return Question.deserialize(this.#question.serialize());
    }

    /** @param {Question} question */
    set question(question) {
        if (this.#question)
            throw new Error(
                "Can't set the question of a PlayQuestionElement more than once."
            );

        this.#question = Question.deserialize(question.serialize());

        this.shadowRoot
            .querySelector("#title")
            .replaceWith(
                new ElementBuilder("h1")
                    .id("title")
                    .child(parseMarkup(this.#question.title.getLocalisation()))
                    .build()
            );

        this.#question.body.forEach((bodySegment) => {
            if (bodySegment instanceof LocalisedString) {
                this.shadowRoot.append(
                    new ElementBuilder("p")
                        .child(parseMarkup(bodySegment.getLocalisation()))
                        .build()
                );
            } else {
                this.shadowRoot.append(
                    new ElementBuilder("div")
                        .class("image-segment")
                        .child(
                            new ElementBuilder("img")
                                .src(bodySegment.url)
                                .alt(bodySegment.alt.getLocalisation())
                        )
                        .child(
                            new ElementBuilder("span")
                                .class("caption")
                                .child(
                                    parseMarkup(
                                        bodySegment.caption.getLocalisation()
                                    )
                                )
                        )
                        .child(
                            new ElementBuilder("span")
                                .class("attribution")
                                .child(
                                    parseMarkup(
                                        bodySegment.attribution.getLocalisation()
                                    )
                                )
                        )
                        .build()
                );
            }
        });

        if (this.#question.answers.length > 0)
            this.shadowRoot.append(
                new ElementBuilder("h2")
                    .id("answers-heading")
                    .child("Answers:")
                    .build()
            );

        const answersDiv = new ElementBuilder("div").id("answers").build();
        this.shadowRoot.append(answersDiv);

        this.#question.answers.forEach((answer) => {
            /** @type {HTMLButtonElement} */
            let answerButton;

            const answerButtonBuilder = new ElementBuilder("button")
                .class("answer")
                .listen("click", () => {
                    if (answer.correct) {
                        answerButton.classList.add("correct");
                    } else {
                        answerButton.classList.add("error");
                    }

                    answerButton.addEventListener("transitionend", (event) => {
                        if (event.propertyName === "box-shadow") {
                            if (!answerButton.classList.contains("end"))
                                setTimeout(
                                    () => answerButton.classList.add("end"),
                                    333
                                );
                            else
                                setTimeout(
                                    () =>
                                        this.dispatchEvent(
                                            new CustomEvent("answered", {
                                                detail: {
                                                    correct: answer.correct,
                                                },
                                            })
                                        ),
                                    333
                                );
                        }
                    });
                });

            if (answer.body instanceof LocalisedString) {
                answerButtonBuilder.child(
                    new ElementBuilder("span").child(
                        answer.body.getLocalisation()
                    )
                );
            } else {
                answerButtonBuilder
                    .child(
                        new ElementBuilder("img")
                            .src(answer.body.url)
                            .alt(answer.body.alt.getLocalisation())
                    )
                    .child(
                        new ElementBuilder("span")
                            .class("caption")
                            .child(answer.body.caption.getLocalisation())
                    )
                    .child(
                        new ElementBuilder("span")
                            .class("attribution")
                            .child(
                                parseMarkup(
                                    answer.body.attribution.getLocalisation()
                                )
                            )
                    );
            }

            answerButton = answerButtonBuilder.build();

            answersDiv.append(answerButton);
        });

        if (
            this.#question.answers.length % 2 === 0 ||
            this.#question.answers.length % 3 !== 0
        ) {
            answersDiv.style.setProperty("--cols", `${2}`);
            answersDiv.style.setProperty(
                "--rows",
                `${Math.ceil(this.#question.answers.length / 2)}`
            );
        } else {
            answersDiv.style.setProperty("--cols", `${3}`);
            answersDiv.style.setProperty(
                "--rows",
                `${Math.ceil(this.#question.answers.length / 3)}`
            );
        }
    }
}

const template = new ElementBuilder("template")
    .child(
        new ElementBuilder("link")
            .rel("stylesheet")
            .type("text/css")
            .href(new URL("../../style/all.css", import.meta.url).toString())
    )
    .child(
        new ElementBuilder("style") //
            .child(`
                p {
                    margin: 1rem 0.5rem 1rem 0;
                    line-height: 1.25;
                }

                .image-segment {
                    --btn-bg-r: calc(var(--r-fg-r) - 20 * var(--soften) - 7.5 * var(--soften-border));
                    --btn-bg-g: calc(var(--r-fg-g) - 20 * var(--soften) - 7.5 * var(--soften-border));
                    --btn-bg-b: calc(var(--r-fg-b) - 20 * var(--soften) - 7.5 * var(--soften-border));

                    margin: 0.25rem 0.5rem 0.25rem 0;

                    max-width: min(100%, 31rem);
                }

                .answer {
                    font-size: 100%;
                    box-shadow: 0 0 0 0 var(--ac);
                    min-height: 4rem;
                    white-space: normal;
                }

                .image-segment,
                #answers > .answer {
                    display: inline-flex;
                    flex-direction: column;

                    border: var(--btn-border);
                    border-radius: var(--border-radius);
                    padding: 0.5rem;

                    gap: 0.25rem;
                }

                .image-segment img,
                .answer img {
                    border: var(--btn-border);
                    border-radius: var(--border-radius);
                    font-family: sans-serif;
                    width: 100vw;
                    max-width: 20rem;
                    max-height: 30rem;
                    object-fit: contain;
                    background:
                        repeating-conic-gradient(
                            rgba(var(--fg-r), var(--fg-g), var(--fg-b), 0.125) 0% 25%,
                            rgba(var(--fg-r), var(--fg-g), var(--fg-b), 0.25) 0% 50%
                        )
                        50% / 20px 20px;
                }

                .answer img {
                    width: 100%;
                }

                .answer:hover img,
                .answer:focus-visible img{
                    opacity: 0.8;
                }

                .answer:active img {
                    opacity: 0.6;
                }

                .image-segment .attribution,
                .answer .attribution {
                    font-size: 75%;
                }

                .image-segment .caption:empty,
                .answer .caption:empty,
                .image-segment .attribution:empty,
                .answer .attribution:empty {
                    display: none;
                }

                .answer > * {
                    margin: auto;
                }

                .answer > :not(:last-child) {
                    margin-bottom: 0;
                }

                .answer > :not(:first-child) {
                    margin-top: 0;
                }

                #answers {
                    --rows: 1;
                    --cols: 1;

                    display: grid;

                    grid-template-columns: repeat(var(--cols), 1fr);
                    grid-template-rows: repeat(var(--rows), 1fr);

                    gap: 0.5rem;
                }

                .answer.correct,
                .answer.error,
                .answer.correct:hover,
                .answer.error:hover,
                .answer.correct:active,
                .answer.error:active {
                    transition: all 0.2s ease, box-shadow 0.33s ease-out;
                    box-shadow: 0 0 0 100vmax var(--ac);
                    z-index: 1;

                    border: var(--btn-a-border);
                    background: var(--btn-a-bg);
                    color: var(--btn-a-fg);
                }

                .answer.correct.end,
                .answer.error.end {
                    box-shadow: 0 0 0 100vmax var(--bg);
                }

                .caption, .attribution {
                    white-space: normal;
                }

                .image-segment > img,
                .image-segment > .caption,
                .image-segment > .attribution {
                    max-width: min(100%, 30rem);
                    max-height: 45rem;
                }
            `)
    )
    .child(new ElementBuilder("h1").id("title").child("Loading question..."))
    .build();

window.customElements.define("qz-play-question", PlayQuestionElement);
