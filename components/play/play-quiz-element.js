import { ElementBuilder } from "../../util/element-builder.js";

import { Quiz } from "../../model/quiz.js";

import { PlayQuestionElement } from "./play-question-element.js";

export class PlayQuizElement extends HTMLElement {
    /** @type {Quiz} */
    #quiz;
    /** @type {number} */
    #questionIndex;
    /** @type {number} */
    #correctAnswers;

    constructor() {
        super();

        this.attachShadow({ mode: "open" });
        [...template.content.children].forEach((templateChild) =>
            this.shadowRoot.appendChild(templateChild.cloneNode(true))
        );
    }

    endPage() {
        const quizURL = `${window.location.protocol}//${
            window.location.host
        }${window.location.pathname.replace(/\/+$/, "")}${
            window.location.search
        }`;

        const shareButton = navigator.share
            ? new ElementBuilder("button")
                  .styleProperty("font-size", "100%")
                  .child(
                      new ElementBuilder("i")
                          .class("fa-solid")
                          .class("fa-share")
                  )
                  .child(" ")
                  .child("Share")
                  .listen("click", () =>
                      navigator.share({
                          url: quizURL,
                          title: this.#quiz.title,
                          text: `i got ${this.#correctAnswers} question${
                              this.#correctAnswers != 1 ? "s" : ""
                          } right on this quiz`,
                      })
                  )
            : new ElementBuilder("div")
                  .child(
                      new ElementBuilder("label")
                          .styleProperty("line-height", "2")
                          .child("Link to this quiz")
                          .for("quiz-link-input")
                  )
                  .child(new ElementBuilder("br"))
                  .child(
                      new ElementBuilder("div")
                          .class("button-group")
                          .style({
                              display: "flex",
                              "flex-direction": "row",
                          })
                          .child(
                              new ElementBuilder("input")
                                  .id("quiz-link-input")
                                  .styleProperty("font-size", "100%")
                                  .styleProperty("text-overflow", "ellipsis")
                                  .styleProperty("margin-left", "0")
                                  .styleProperty("flex-grow", "1")
                                  .listen("click", function () {
                                      this.select();
                                  })
                                  .value(quizURL)
                          )
                          .child(
                              new ElementBuilder("button")
                                  .styleProperty("font-size", "100%")
                                  .styleProperty("min-width", "10ch")
                                  .child(
                                      new ElementBuilder("i")
                                          .class("fa-solid")
                                          .class("fa-clipboard")
                                          .styleProperty("margin-right", "1ch")
                                  )
                                  .child("copy")
                                  .listen("click", function () {
                                      this.closest(".button-group")
                                          .querySelector("input")
                                          .select();
                                      document.execCommand("copy");
                                      this.focus();

                                      this.querySelector("i").classList.remove(
                                          "fa-clipboard"
                                      );
                                      this.querySelector("i").classList.add(
                                          "fa-check"
                                      );

                                      this.innerHTML = this.innerHTML.replace(
                                          "copy",
                                          "copied"
                                      );

                                      setTimeout(() => {
                                          this.querySelector(
                                              "i"
                                          ).classList.remove("fa-check");
                                          this.querySelector("i").classList.add(
                                              "fa-clipboard"
                                          );

                                          this.innerHTML =
                                              this.innerHTML.replace(
                                                  "copied",
                                                  "copy"
                                              );
                                      }, 1000);
                                  })
                          )
                  );

        this.shadowRoot.append(
            new ElementBuilder("div")
                .style({
                    "text-align": "center",
                    display: "flex",
                    "flex-direction": "column",
                    "align-items": "stretch",
                    gap: "1rem",
                    width: "fit-content",
                    margin: "0 auto",
                })
                .child(
                    new ElementBuilder("h1")
                        .child("You got")
                        .child(new ElementBuilder("br"))
                        .child(
                            new ElementBuilder("big")
                                .styleProperty("font-size", "500%")
                                .styleProperty(
                                    "filter",
                                    `hue-rotate(${
                                        -140 *
                                        (1 -
                                            this.#correctAnswers /
                                                this.#quiz.questions.length)
                                    }deg) saturate(${
                                        2 -
                                        this.#correctAnswers /
                                            this.#quiz.questions.length
                                    })`
                                )
                                .styleProperty("color", "var(--ac)")
                                .child(`${this.#correctAnswers}`)
                                .child("/")
                                .child(`${this.#quiz.questions.length}`)
                        )
                        .child(new ElementBuilder("br"))
                        .child(
                            `question${
                                this.#correctAnswers != 1 ? "s" : ""
                            } right!`
                        )
                )
                .child(shareButton)
                .child(
                    new ElementBuilder("button")
                        .child(
                            new ElementBuilder("i")
                                .class("fa-solid")
                                .class("fa-arrow-rotate-left")
                                .styleProperty("margin-right", "1ch")
                        )
                        .child("Play again")
                        .styleProperty("font-size", "100%")
                        .listen("click", () => window.location.reload())
                )
                .child(
                    new ElementBuilder("button")
                        .child(
                            new ElementBuilder("i")
                                .class("fa-solid")
                                .class("fa-pen")
                                .styleProperty("margin-right", "1ch")
                        )
                        .child("Make your own quiz")
                        .styleProperty("font-size", "100%")
                        .listen("click", () => window.location.assign("/edit"))
                )
                .child(
                    new ElementBuilder("button")
                        .child(
                            new ElementBuilder("i")
                                .class("fa-solid")
                                .class("fa-pen-to-square")
                                .styleProperty("margin-right", "1ch")
                        )
                        .child("Edit a copy")
                        .styleProperty("font-size", "100%")
                        .listen("click", () =>
                            window.location.assign(
                                `/edit${window.location.search}`
                            )
                        )
                )
                .build()
        );
    }

    nextQuestion() {
        this.shadowRoot
            .querySelector("qz-play-question.active")
            ?.classList.replace("active", "done");

        const questionEls = [
            ...this.shadowRoot.querySelectorAll("qz-play-question:not(.done)"),
        ];

        if (questionEls.length === 0) return this.endPage();

        /** @type {PlayQuestionElement} */
        let nextQuestionEl;

        if (
            Number.isInteger(this.#quiz.shuffleFrom) &&
            this.#questionIndex >= this.#quiz.shuffleFrom
        ) {
            nextQuestionEl =
                questionEls[Math.floor(Math.random() * questionEls.length)];
        } else {
            nextQuestionEl = questionEls[0];
        }

        nextQuestionEl.classList.add("active");
        nextQuestionEl.addEventListener(
            "answered",
            ({ detail: { correct } }) => {
                if (correct) this.#correctAnswers++;

                this.nextQuestion();
            },
            { once: true }
        );

        this.#questionIndex++;
    }

    /** @returns {Quiz} */
    get quiz() {
        return Quiz.deserialize(this.#quiz.serialize());
    }

    /** @param {Quiz} quiz */
    set quiz(quiz) {
        this.#quiz = Quiz.deserialize(quiz.serialize());
        this.#questionIndex = 0;
        this.#correctAnswers = 0;

        [...this.children].forEach((child) => child.remove());

        this.#quiz.questions.forEach((question) => {
            this.shadowRoot.append(
                new ElementBuilder(PlayQuestionElement)
                    .question(question)
                    .build()
            );
        });

        this.nextQuestion();
    }
}

const template = new ElementBuilder("template")
    .child(
        new ElementBuilder("link")
            .rel("stylesheet")
            .type("text/css")
            .href(new URL("../../style/all.css", import.meta.url).toString())
    )
    .child(
        new ElementBuilder("style") //
            .child(`
                qz-play-question:not(.active) {
                    display: none;
                }
            `)
    )
    .build();

window.customElements.define("qz-play-quiz", PlayQuizElement);
