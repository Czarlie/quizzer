import { lookup, shorten } from "../util/shortener.js";
import { Quiz } from "../model/quiz.js";
const quizElement = document.querySelector("qz-quiz");

let origJSON = "";

const loadQuiz = () => {
    if (window.location.search.replace(/^\?/, "")) {
        lookup(window.location.search.replace(/^\?/, "")).then((json) => {
            if (!json) return;

            origJSON = JSON.stringify(json);
            quizElement.value = Quiz.deserialize(json);
        });
    }
};

loadQuiz();

document.querySelector("#save").addEventListener("click", () =>
    shorten(quizElement.value.serialize()).then((key) => {
        document.documentElement.classList.remove("unsaved");

        history.replaceState(null, "", `?${key}`);
    })
);

document.querySelector("#play").addEventListener("click", () =>
    shorten(quizElement.value.serialize()).then((key) => {
        document.documentElement.classList.remove("unsaved");

        history.replaceState(null, "", `?${key}`);
        window.location = `/?${key}`;
    })
);

quizElement.addEventListener("change", () => {
    document.documentElement.classList.add("unsaved");
});

window.addEventListener("beforeunload", (e) => {
    if (
        document.documentElement.classList.contains("unsaved") &&
        JSON.stringify(quizElement.value.serialize()) !== origJSON
    ) {
        e.preventDefault();
        e.returnValue = "";
    }
});
