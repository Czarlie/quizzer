import { Question } from "./question.js";
import { LocalisedString } from "./localised-string.js";

import { Serialisable } from "./serialisable.js";

/**
 * @typedef {import('./image.js').QuestionObject} QuestionObject
 */
/**
 * @typedef {import('./localised-string.js').LocalisedStringObject} LocalisedStringObject
 */
/**
 * @typedef {object} QuizObject
 *
 * @property {LocalisedStringObject} title
 * @property {QuestionObject[]} body
 * @property {number?} [shuffleFrom]
 */

/**
 * A quiz, with a title, any amount of questions and, optionally, the index from which on to shuffle questions.
 *
 * @implements {Serialisable}
 */
export class Quiz extends Serialisable {
    /**
     * The title of the quiz
     * @type {LocalisedString}
     */
    #title;
    /**
     * The questions that make up this Quiz
     * @type {Question[]}
     */
    #questions;
    /**
     * The index from which on to shuffle questions, or null (=no shuffling)
     * @type {number?}
     */
    #shuffleFrom;

    /**
     * Create a new Quiz.
     * @param {QuizObject | null} initialValue
     *     The value to set this Quiz to
     */
    constructor(initialValue = null) {
        super();

        if (initialValue) {
            this.#title = new LocalisedString(initialValue.title);

            this.#questions = initialValue.questions.map((questionObject) => {
                return new Question(questionObject);
            });

            this.#shuffleFrom = Number.isInteger(initialValue.shuffleFrom)
                ? initialValue.shuffleFrom
                : null;
        } else {
            this.#title = new LocalisedString("");
            this.#questions = [];
            this.#shuffleFrom = null;
        }
    }

    /**
     * Create a new Quiz from its serialisation.
     * @param {QuizObject} serialisation
     *     The value to set this Quiz to
     * @returns {Quiz} The deserialized Quiz instance
     */
    static deserialize(serialisation) {
        return new this(serialisation);
    }

    /**
     * Serialise to JSON-compatible types.
     * @return {QuizObject} A JSON-compatible serialisation
     */
    serialize() {
        /** @type {QuizObject} */
        const quizObject = {
            title: this.#title.serialize(),
            questions: this.#questions.map((question) => question.serialize()),
        };

        if (Number.isInteger(this.#shuffleFrom))
            quizObject.shuffleFrom = this.#shuffleFrom;

        return quizObject;
    }

    get title() {
        return LocalisedString.deserialize(this.#title.serialize());
    }
    get questions() {
        return this.#questions.map((question) =>
            Question.deserialize(question.serialize())
        );
    }
    get shuffleFrom() {
        return this.#shuffleFrom;
    }
}
