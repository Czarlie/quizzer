import { LocalisedString } from "./localised-string.js";
import { Serialisable } from "./serialisable.js";

/**
 * @typedef {import('./localised-string.js').LocalisedStringObject} LocalisedStringObject
 */
/**
 * @typedef {object} ImageObject
 *
 * @property {"image"} type
 * @property {string | URL} url
 * @property {LocalisedStringObject} [alt]
 * @property {LocalisedStringObject} [caption]
 * @property {LocalisedStringObject} [attribution]
 */

/**
 * An image, specified by url, including alt text, caption and attribution.
 *
 * @implements {Serialisable}
 */
export class Image extends Serialisable {
    /**
     * The URL to the image
     * @type {string}
     */
    #url;
    /**
     * An alt text for the image
     * @type {LocalisedString}
     */
    #alt;
    /**
     * The caption to show for the image
     * @type {LocalisedString}
     */
    #caption;
    /**
     * Attribution for the image
     * @type {LocalisedString}
     */
    #attribution;

    /**
     * Create a new Image.
     * @param {ImageObject | null} initialValue
     *     The value to set this Image to
     */
    constructor(initialValue = null) {
        super();

        if (initialValue) {
            this.#url = initialValue.url.toString();
            this.#alt = LocalisedString.deserialize(initialValue.alt ?? "");
            this.#caption = LocalisedString.deserialize(
                initialValue.caption ?? ""
            );
            this.#attribution = LocalisedString.deserialize(
                initialValue.attribution ?? ""
            );
        } else {
            this.#url = "";
            this.#alt = new LocalisedString("");
            this.#caption = new LocalisedString("");
            this.#attribution = new LocalisedString("");
        }
    }

    /**
     * Create a new Image from its serialisation.
     * @param {ImageObject} serialisation
     *     The value to set this Image to
     * @returns {Image} The deserialized Image instance
     */
    static deserialize(serialisation) {
        return new this(serialisation);
    }

    /**
     * Serialise to JSON-compatible types.
     * @return {ImageObject} A JSON-compatible serialisation
     */
    serialize() {
        const obj = {
            type: "image",
            url: this.#url,
        };

        if (!this.#alt.isEmpty()) {
            obj.alt = this.#alt.serialize();
        }
        if (!this.#caption.isEmpty()) {
            obj.caption = this.#caption.serialize();
        }
        if (!this.#attribution.isEmpty()) {
            obj.attribution = this.#attribution.serialize();
        }

        return obj;
    }

    get url() {
        return this.#url;
    }
    get alt() {
        return LocalisedString.deserialize(this.#alt.serialize());
    }
    get caption() {
        return LocalisedString.deserialize(this.#caption.serialize());
    }
    get attribution() {
        return LocalisedString.deserialize(this.#attribution.serialize());
    }
}
