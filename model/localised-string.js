import { Serialisable } from "./serialisable.js";

/**
 * @typedef {(
 *     {
 *         type: "string",
 *         [lang: string]: string,
 *     } |
 *     string
 * )} LocalisedStringObject
 */

/**
 * A string or an object that maps a subset of IETF language tags to strings in that language.
 *
 * Allowed language tags consist of:
 *
 * -   a two-to-three-letter ISO 639-1 primary language subtag
 * -   [optional] A region subtag, either a two-letter ISO 3166-1 alpha-2 country code or
 *     a three-digit UN M.49 code
 *
 * The default string may be specified under the key "" and will be used if the preferred
 * language(s) aren't available.
 *
 * @implements {Serialisable}
 */
export class LocalisedString extends Serialisable {
    /**
     * An object mapping language codes (or "") to localised strings.
     * @type {{[lang: string]: string}}
     */
    #languages;

    /**
     * Create a new LocalisedString.
     * @param {LocalisedStringObject | null} initialValue
     *     The value to set this LocalisedString to
     */
    constructor(initialValue) {
        super();

        this.#languages = {};

        if (typeof initialValue === "string")
            this.#languages[""] = initialValue;
        else if (initialValue) {
            Object.entries(initialValue).forEach(([lang, value]) => {
                if (lang === "type") return;
                this.setLocalisation(lang, value);
            });
        } else {
            this.#languages[""] = "";
        }

        if (!("" in this.#languages)) {
            throw new Error("Must include a default text (for language '')");
        }
    }

    /**
     * Create a new LocalisedString from its serialisation.
     * @param {string | {[lang: string]: string}} serialisation
     *     The value to set this LocalisedString to
     * @returns {LocalisedString} The deserialized LocalisedString instance
     */
    static deserialize(serialisation) {
        return new this(serialisation);
    }

    /**
     * Serialise to JSON-compatible types.
     * @return {LocalisedStringObject} A JSON-compatible serialisation
     */
    serialize() {
        if (Object.keys(this.#languages).length === 1 && "" in this.#languages)
            return this.#languages[""];

        return { type: "string", ...this.#languages };
    }

    /**
     * Set the localisation for a language tag.
     * @param {string} lang  The language tag to set the localisation for
     * @param {string} value The localisation for that language
     */
    setLocalisation(lang, value) {
        if (!this.constructor.checkLangTag(lang))
            throw new Error(`Language tag "${lang}" is not valid`);

        this.#languages[lang] = value;
    }

    /**
     * Get the localisation for a language tag.
     *
     * Fall back to only the primary language subtag if a region subtag is specified,
     * and then the default `""`.
     *
     * If all fallbacks fail, return `""`.
     *
     * @param  {string} [lang] The language tag to get the localisation for
     * @return {string} The localisation for the passed language, hopefully.
     */
    getLocalisation(lang) {
        if (!lang) {
            for (const preferredLang of navigator.languages) {
                if (preferredLang in this.#languages) {
                    lang = preferredLang;
                    break;
                }
                if (preferredLang.split("-", 1) in this.#languages) {
                    lang = preferredLang.split("-", 1);
                    break;
                }
            }
            lang = "";
        }

        if (!this.constructor.checkLangTag(lang))
            throw new Error(`Language tag "${lang}" is not valid`);

        if (lang in this.#languages) return this.#languages[lang];

        /**
         * The two-to-three-letter primary language subtag of the passed tag
         *
         * This is `""` if the passed tag was also `""`
         *
         * @type {string}
         */
        const primaryLanguageSubtag = lang.split("-", 1);

        if (primaryLanguageSubtag in this.#languages)
            return this.#languages[primaryLanguageSubtag];

        if ("" in this.#languages) return this.#languages[""];

        return "";
    }

    /**
     * Get all language tags for which a localisation is defined.
     * @return {string[]}
     */
    getLanguages() {
        return Object.keys(this.#languages).sort();
    }

    /** @return {boolean} */
    isEmpty() {
        if (Object.keys(this.#languages).length === 0) return true;
        if (Object.keys(this.#languages).length > 1) return false;
        if (!("" in this.#languages)) return false;
        if (this.#languages[""] === "") return true;
        return false;
    }

    /**
     * Check whether a language tag is allowed as a localisation key.
     * @param  {string} tag The tag to check
     * @return {boolean} Whether the tag is valid
     */
    static checkLangTag(tag) {
        if (tag === "") return true;
        if (/^[a-z]{2,3}$/.test(tag)) return true; // aa
        if (/^[a-z]{2,3}-[A-Z]{2}$/.test(tag)) return true; // aa-AA
        if (/^[a-z]{2,3}-\d{3}$/.test(tag)) return true; // aa-000

        return false;
    }
}
