import { Image } from "./image.js";
import { LocalisedString } from "./localised-string.js";

import { Serialisable } from "./serialisable.js";

/**
 * @typedef {import('./image.js').ImageObject} ImageObject
 */
/**
 * @typedef {import('./localised-string.js').LocalisedStringObject} LocalisedStringObject
 */
/**
 * @typedef {object} AnswerObject
 *
 * @property {boolean} correct
 * @property {LocalisedStringObject | ImageObject} body
 */

/**
 * An answer to a question, consisting of a localised string or an image.
 *
 * An answer also includes whether the answer is correct
 *
 * @implements {Serialisable}
 */
export class Answer extends Serialisable {
    /**
     * Whether the answer is correct
     * @type {boolean}
     */
    #correct;
    /**
     * The answer content, either a localised string or an image
     * @type {LocalisedString | Image}
     */
    #body;

    /**
     * Create a new Answer.
     * @param {AnswerObject | null} initialValue
     *     The value to set this Answer to
     */
    constructor(initialValue = null) {
        super();

        if (initialValue) {
            this.#correct = initialValue.correct;

            if (
                typeof initialValue.body === "string" ||
                initialValue.body.type === "string"
            )
                this.#body = LocalisedString.deserialize(initialValue.body);
            else this.#body = Image.deserialize(initialValue.body);
        } else {
            this.#correct = false;
            this.#body = new LocalisedString("");
        }
    }

    /**
     * Create a new Answer from its serialisation.
     * @param {AnswerObject} serialisation
     *     The value to set this Answer to
     * @returns {Answer} The deserialized Answer instance
     */
    static deserialize(serialisation) {
        return new this(serialisation);
    }

    /**
     * Serialise to JSON-compatible types.
     * @return {AnswerObject} A JSON-compatible serialisation
     */
    serialize() {
        return {
            correct: this.#correct,
            body: this.#body.serialize(),
        };
    }

    get correct() {
        return this.#correct;
    }
    get body() {
        return this.#body.constructor.deserialize(this.#body.serialize());
    }
}
