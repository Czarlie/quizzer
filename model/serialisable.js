/**
 * A base class that defines the structure of classes that allow "serialisation", i.e. conversion
 * to JSON-compatible types and "deserialisation".
 *
 * @interface
 */
export class Serialisable {
    /**
     * Create a new instance of this class from its JSON-compatible serialisation
     * @param  {*} serialisation JSON-compatible types that describe an object of this class
     * @return {Serialisable} A new instance that wraps the passed data
     */
    static deserialize(serialisation) {
        throw new Error(
            `This Serialisable class (${this.name}) didn't implement the static deserialize() method`
        );
    }
    /**
     * Get a JSON-compatible value that this object can be re-constructed from.
     * @return {*} A JSON-compatible description of this object
     */
    serialize() {
        throw new Error(
            `This Serialisable class (${this.constructor.name}) didn't implement the serialize() method`
        );
    }
}
