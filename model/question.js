import { Answer } from "./answer.js";
import { Image } from "./image.js";
import { LocalisedString } from "./localised-string.js";

import { Serialisable } from "./serialisable.js";

/**
 * @typedef {import('./image.js').ImageObject} ImageObject
 */
/**
 * @typedef {import('./localised-string.js').LocalisedStringObject} LocalisedStringObject
 */
/**
 * @typedef {import('./answer.js').AnswerObject} AnswerObject
 */
/**
 * @typedef {object} QuestionObject
 *
 * @property {LocalisedStringObject} title
 * @property {(LocalisedStringObject | ImageObject)[]} [body]
 * @property {AnswerObject[]} answers
 */

/**
 * A question, with a title, any amount of body segments and two or more answers.
 *
 * @implements {Serialisable}
 */
export class Question extends Serialisable {
    /**
     * The title of the question
     * @type {LocalisedString}
     */
    #title;
    /**
     * The question content, any amount of localised strings and/or images
     * @type {(LocalisedString | Image)[]}
     */
    #body;
    /**
     * Answer options to the question, any amount of which correct
     * @type {Answer[]}
     */
    #answers;

    /**
     * Create a new Question.
     * @param {QuestionObject | null} initialValue
     *     The value to set this Question to
     */
    constructor(initialValue = null) {
        super();

        if (initialValue) {
            this.#title = new LocalisedString(initialValue.title);

            this.#body = (initialValue.body ?? []).map((bodySegmentObject) => {
                if (
                    typeof bodySegmentObject === "string" ||
                    bodySegmentObject.type === "string"
                )
                    return new LocalisedString(bodySegmentObject);
                return new Image(bodySegmentObject);
            });

            this.#answers = initialValue.answers.map(
                (answerObject) => new Answer(answerObject)
            );
        } else {
            this.#title = new LocalisedString("");
            this.#body = [];
            this.#answers = [];
        }
    }

    /**
     * Create a new Question from its serialisation.
     * @param {QuestionObject} serialisation
     *     The value to set this Question to
     * @returns {Question} The deserialized Question instance
     */
    static deserialize(serialisation) {
        return new this(serialisation);
    }

    /**
     * Serialise to JSON-compatible types.
     * @return {QuestionObject} A JSON-compatible serialisation
     */
    serialize() {
        /** @type {QuestionObject} */
        const questionObject = {
            title: this.#title.serialize(),
            answers: this.#answers.map((answer) => answer.serialize()),
        };

        if (this.#body.length > 0)
            questionObject.body = this.#body.map((bodySegment) =>
                bodySegment.serialize()
            );

        return questionObject;
    }

    /** @return {LocalisedString} */
    get title() {
        return LocalisedString.deserialize(this.#title.serialize());
    }
    /** @return {(LocalisedString | Image)[]} */
    get body() {
        return this.#body.map((bodySegment) =>
            bodySegment.constructor.deserialize(bodySegment.serialize())
        );
    }
    /** @return {Answer[]} */
    get answers() {
        return this.#answers.map((answer) =>
            Answer.deserialize(answer.serialize())
        );
    }
}
